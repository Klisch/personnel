package Project;
import java.util.regex.*;

public class TestRegexp {

    public static final Pattern pattern = Pattern.compile
            ("[a-zA-Z]{1}[a-zA-Z\\d\u002E\u005F]+@([a-zA-Z]+\u002E)([a-z]+)");

    public static void doMatch(String word) {
        String output = "Validation for " + word;
        Matcher matcher = pattern.matcher(word);
        if (matcher.matches())
            output += " passed.";
        else
            output += " not passed.";
        System.out.println(output);
    }

    public static void main(String[] args) {
        doMatch("c0nst@money.net");
        doMatch("somebody@dev.ua");
        doMatch("Name.Sur_name@gmail.com");
        doMatch("useR33@somewhere.net");
    }

}
