package Project.DAO;

import Project.model.Group;
import Project.model.User;

import java.util.List;


public interface GroupDao {
     List<Group> getGroups(User user);
     Group get(int id);
     void saveGroup(String name,  String login);
     void deleteGroup(int id);
}
