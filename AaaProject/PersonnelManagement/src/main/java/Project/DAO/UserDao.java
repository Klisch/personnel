package Project.DAO;


import Project.model.User;

public interface UserDao {
    void saveUser(User user);
    User getUser(String login);
    User getUser(int id);
    boolean isUserExist(String login,String password);
    void deleteUser(String login);

}
