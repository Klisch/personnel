package Project.DAO;

import Project.Util.JdbcWrapper;
import Project.mappers.UserMapper;
import Project.model.Group;
import Project.model.User;

import java.util.List;


public class GroupDaoImpl implements GroupDao {

    private final static GroupDaoImpl GROUPDAO=new GroupDaoImpl();

    private GroupDaoImpl(){}

    public static GroupDaoImpl getGROUPDAO() {
        return GROUPDAO;
    }

    @Override
    public List<Group> getGroups(User user) {
        return null;
    }

    @Override
    public Group get(int id) {
        return null;
    }

    @Override
    public void saveGroup(String name, String login) {
        List<User> user=JdbcWrapper.find("select * from users where login=? ",
                new UserMapper(),
                login);
        JdbcWrapper.update("insert into groups (name, idAdmin) values(?,?)",
                name,
                user.get(0).getId());
    }

    @Override
    public void deleteGroup(int id) {

    }
}
