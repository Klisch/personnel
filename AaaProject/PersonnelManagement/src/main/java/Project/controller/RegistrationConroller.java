package Project.controller;


import Project.DAO.UserDao;
import Project.DAO.UserDaoImpl;
import Project.Form.RegistrationForm;
import Project.Util.FormUtil;
import Project.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@WebServlet("/registration")
public class RegistrationConroller extends HttpServlet  {

    public UserDao userDao=UserDaoImpl.getUSERDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/withoutAuthorization/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RegistrationForm registrationForm= FormUtil.readForm(req,RegistrationForm.class);
        List<String> errors = validate(registrationForm);
        if (errors.size()>0){
            req.setAttribute("errors",errors);
            req.getRequestDispatcher("/WEB-INF/withoutAuthorization/registration.jsp").forward(req, resp);
        }
        else {
            User user=formToUser(registrationForm);
            userDao.saveUser(user);//Exeption MySQLIntegrityConstraintViolationException
            resp.sendRedirect("/mainPage");
        }
    }

    private User formToUser(RegistrationForm registrationForm){
        return new User(null,
                registrationForm.getFirstName(),
                registrationForm.getSecondName(),
                registrationForm.getLogin(),
                registrationForm.getPassword());
    }
    private List<String> validate(RegistrationForm registrationForm){
        List<String> errors = new ArrayList<>();

        Pattern pattern=Pattern.compile("[a-zA-Z]{1}[a-zA-Z\\d\u002E\u005F]+@([a-zA-Z]+\u002E)([a-z]+)");
        Matcher matcher=pattern.matcher(registrationForm.getLogin());
        if (!matcher.matches()){
            errors.add("incorrectly E-mail");
        }

        if (!registrationForm.getPassword().equals(registrationForm.getPasswordConfirmation())) {
            errors.add("Password confirmation should match password");
        }
        return errors;
    }
   /* private List<String> validateLogin(RegistrationForm registrationForm) {
        List<String> errors =  new ArrayList<>();
        Pattern pattern = Pattern.compile("[a-zA-Z]{1}[a-zA-Z\\d\u002E\u005F]+@([a-zA-Z]+\u002E)([a-z]+)");
        Matcher matcher = pattern.matcher(registrationForm.getLogin());
        if (!matcher.matches()) {
            errors.add("incorrectly E-mail");
        }
        return errors;
    }*/
}
