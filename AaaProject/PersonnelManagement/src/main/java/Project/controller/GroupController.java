package Project.controller;

import Project.DAO.GroupDaoImpl;
import Project.Form.GroupForm;
import Project.Util.FormUtil;
import Project.model.Group;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/group")
public class GroupController extends HttpServlet {
    GroupDaoImpl GROUPDAO=GroupDaoImpl.getGROUPDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/withAuthorization/Group/group.jsp").forward(request,response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        GroupForm form= FormUtil.readForm(request,GroupForm.class);
        String login= (String) request.getSession().getAttribute("currentUser");
        GROUPDAO.saveGroup(form.getName(),login);

        request.getRequestDispatcher("/WEB-INF/withAuthorization/Group/group.jsp").forward(request, response);
    }
}
