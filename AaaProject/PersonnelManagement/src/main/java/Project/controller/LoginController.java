package Project.controller;


import Project.DAO.UserDao;
import Project.DAO.UserDaoImpl;
import Project.Form.LoginForm;
import Project.Util.FormUtil;
import Project.filters.AuthenticationUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginController extends HttpServlet {

    public UserDao userDao= UserDaoImpl.getUSERDAO();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/withoutAuthorization/login.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        LoginForm loginForm= FormUtil.readForm(req,LoginForm.class);

        if(!userDao.isUserExist(loginForm.getLogin(),loginForm.getPassword())){
            req.setAttribute("error","User not exist");
            req.getRequestDispatcher("/WEB-INF/withoutAuthorization/login.jsp").forward(req,resp);
        }
        else {
            AuthenticationUtil.authenticate(req,loginForm.getLogin());
            resp.sendRedirect("/mainPage");
        }
    }
}
