package Project.filters;

import javax.servlet.http.HttpServletRequest;


public class AuthenticationUtil {

    public static void authenticate(HttpServletRequest request, String logiin ){

        request.getSession().setAttribute("currentUser",logiin);
    }

    public static boolean isAuthenticate(HttpServletRequest request){

        return request.getSession().getAttribute("currentUser")!=null;
    }

    public static void logout(HttpServletRequest request){
        request.getSession().invalidate();
    }
}
