package Project.Util;


import Project.mappers.Mapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JdbcWrapper {
    static {
        try {
            Class.forName(PropertiesUtil.getProperty("db.driver"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConection() throws SQLException {
        String url=PropertiesUtil.getProperty("db.url");
        String username=PropertiesUtil.getProperty("db.username");
        String password=PropertiesUtil.getProperty("db.password");

        return DriverManager.getConnection(url,username,password);
    }

    public static void update(String sql, Object... params){
       try (
           Connection connection=getConection();
           PreparedStatement statement=connection.prepareStatement(sql)){
           for (int i = 0; i < params.length; i++) {
               Object param = params[i];
               statement.setObject(i + 1, param);
           }
           statement.executeUpdate();

       }catch (SQLException e){
           e.printStackTrace();
       }
    }

    public static <T> List<T> find(String sql, Mapper<T> mapper, Object ...params){
        List<T> result=new ArrayList<>();
        try(
        Connection connection=getConection();
        PreparedStatement statement=connection.prepareStatement(sql)
        )  {
            for (int i = 0; i <params.length ; i++) {
                Object param = params[i];
                statement.setObject(i+1,param);
            }
            ResultSet rezset=statement.executeQuery();
            while (rezset.next()){
                result.add(mapper.map(rezset));
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return result;
    }

    public static List<Integer> insert(String sql, Object ...params){
        List<Integer> idGet=new LinkedList<>();

        try(Connection connection=getConection();
        PreparedStatement statement=connection.prepareStatement(sql);){
            for (int i = 0; i <params.length ; i++) {
                Object param=params[i];
                statement.setObject(i+1,param);
            }
            ResultSet result=statement.getGeneratedKeys();
            while (result.next()){
                idGet.add(result.getInt(1));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return idGet;
    }
}
