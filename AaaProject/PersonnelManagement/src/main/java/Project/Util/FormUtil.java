package Project.Util;


import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class FormUtil {

    public static <T> T readForm(HttpServletRequest request, Class<T> formClass) {
        T result = null;

        try {
            Field[] fields = formClass.getDeclaredFields();
            Constructor<T> constructor = formClass.getConstructor();
            result = constructor.newInstance();

            for (Field f : fields) {
                String fieldName = f.getName();
                String stringValue = request.getParameter(fieldName);

                Object value=null;
                Class expectedType=f.getType();
                if (String.class.equals(expectedType)){
                    value=stringValue;
                }
                else if(Integer.class.equals(expectedType) || int.class.equals(expectedType)){
                    value=Integer.parseInt(stringValue);
                }

                f.setAccessible(true);
                f.set(result, value);
                f.setAccessible(false);
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
        return result;
    }
}
