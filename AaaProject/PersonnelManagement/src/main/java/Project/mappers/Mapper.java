package Project.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by 123 on 16.06.2015.
 */
public interface Mapper<T> {
    T map(ResultSet resultSet) throws SQLException;
}
