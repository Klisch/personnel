<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
  <title>Registration</title>
</head>
<body>

<h2>Registration</h2>
<c:if test="${not empty errors}">
<ul>
  <c:forEach items="${errors}" var="error">
    <li>${error}</li>
  </c:forEach>
</ul>
</c:if>
<form action="/registration"method="POST">
  <div>
    First name <br><input type="text"name="firstName" required placeholder="Enter yours  first name " >
  </div>
  <div>
    Second name <br><input type="text"name="secondName" required placeholder="Enter yours second name" >
  </div>
  <div>
    Login <br><input type="text"name="login" required placeholder="Enter yours E-mail" >
  </div>
  <div>
    Password <br><input type="password" name="password" required placeholder="Entery yours password">
  </div>
  <div>
    Password <br><input type="password" name="passwordConfirmation" required placeholder="Entery yours password">
  </div>
  <div>
    <button type="submit">Registration</button>
  </div>
</form>

<jsp:include page="/WEB-INF/shared/_footer.jsp"/>
