<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="title" value="Log in" />
<jsp:include page="/WEB-INF/shared/_header.jsp" />

<h2>Login</h2>
  <c:if test="${not empty errors}">
  <ul>
    <c:forEach items="${errors}" var="error">
      <li>${error}</li>
    </c:forEach>
  </ul>
  </c:if>
  <form action="login"method="post">

    <div>
      Login <br><input type="text"name="login" required placeholder="Enter yours login" >
    </div>
    <div>
      Password <br><input type="password" name="password" required placeholder="Entery yours password">
    </div>
      <button type="submit">Login</button>
    </div>
  </form>

<jsp:include page="/WEB-INF/shared/_footer.jsp"/>
